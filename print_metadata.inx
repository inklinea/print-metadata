<?xml version="1.0" encoding="UTF-8"?>


<!--Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]-->

<!--        This program is free software; you can redistribute it and/or modify-->
<!--        it under the terms of the GNU General Public License as published by-->
<!--        the Free Software Foundation; either version 2 of the License, or-->
<!--        (at your option) any later version.-->

<!--        This program is distributed in the hope that it will be useful,-->
<!--        but WITHOUT ANY WARRANTY; without even the implied warranty of-->
<!--        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the-->
<!--        GNU General Public License for more details.-->

<!--        You should have received a copy of the GNU General Public License-->
<!--        along with this program; if not, write to the Free Software-->
<!--        Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.-->


<!--        #############################################################################-->
<!--        #  Print Metadata - Print Inkscape document metadata on canvas   -->
<!--        #  An Inkscape 1.2.1+ extension -->
<!--        # Appears under Extensions>Text>Print Metadata-->
<!--        #############################################################################-->

<inkscape-extension xmlns="http://www.inkscape.org/namespace/inkscape/extension">
    <name>Print Metadata</name>
    <id>inklinea.print_metadata</id>

    <!--  Parameters Here -->

    <label appearance="header">If the first selected object is a path</label>
    <label appearance="header">then a textpath will be created.</label>

    <param name="font_size_float" type="float" precision="2" min="0.1" max="999" gui-text="Font Size">10</param>
    <separator/>
    <param name="datetimestamp_cb" type="bool" gui-text="Include Timestamp">true</param>

    <effect>
        <object-type>all</object-type>
        <effects-menu>
                <submenu name="Text"/>
        </effects-menu>
    </effect>
    <script>
        <command location="inx" interpreter="python">print_metadata.py</command>
    </script>
</inkscape-extension>
