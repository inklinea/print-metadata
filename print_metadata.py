#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Print Metadata - Print Inkscape document metadata on canvas
# An Inkscape 1.2.1+ extension
# Appears under Extensions>Text>Print Metadata
##############################################################################

import inkex
from inkex import TextPath, TextElement, Tspan, Layer

import sys, os
from datetime import datetime
from collections import OrderedDict


def get_metadata(self):
    # Metadata can be in any order in the file
    # Make an indexed list so we can build the ordered dict later
    # From Python 3.7+, normal dictionaries should retain order anyway :)
    metadata_field_names = ['title', 'date', 'creator', 'rights', 'publisher', 'identifier', 'source', 'relation',
                            'language', 'subject', 'coverage', 'description', 'contributor']

    metadata_dict = {}
    metadata_ordered_dict = OrderedDict()

    metadata = self.svg.metadata

    if len(metadata) < 1:
        inkex.errormsg('No Metadata Found')
        return metadata_ordered_dict

    for item in metadata[0]:
        for field in item:

            text_list = field.xpath('.//text()')
            stripped_list = [x.strip() for x in text_list]

            field_text = ''.join(stripped_list)

            if field_text.strip() != '' and field_text is not None:
                metadata_dict[str(field)] = field_text

    for field_name in metadata_field_names:
        if field_name in metadata_dict.keys():
            metadata_ordered_dict[field_name] = metadata_dict[field_name]
            metadata_dict.pop(field_name)

    # Check if the original dict is empty
    if len(metadata_dict) > 0:
        for key, value in metadata_dict.items():
            metadata_ordered_dict[key] = value


    return metadata_ordered_dict


def create_text(self, metadata_dict, path):
    metadata_string = ''

    text_element = TextElement()
    text_element.style['font-size'] = self.font_size
    # Let horizontal offset be 10%
    horizontal_offset = self.font_size / 10
    # Let vertical spacing be 10%
    vertical_spacing = self.font_size / 10

    if self.options.datetimestamp_cb:
        if hasattr(self, 'file_timestamp'):
            metadata_dict['Modified'] = self.file_timestamp

    if len(metadata_dict) < 1:
        sys.exit()

    if path != None:

        for key, value in metadata_dict.items():
            field_string = (f'{key} {value}')
            metadata_string += f' {field_string}'

        path_dupe = path.duplicate()

        path_id = path_dupe.get_id()

        defs = self.svg.defs

        defs.append(path_dupe)

        textpath = TextPath()

        textpath.text = metadata_string

        textpath.href = path_id
        text_element.append(textpath)

        text_element.transform = text_element.transform @ path.composed_transform()

    else:
        # SVG text does not have line breaks
        # <tspan> required for each line
        tspan_y = self.font_size
        for key, value in metadata_dict.items():
            field_string = (f'{key.capitalize()} : {value}')
            tspan = Tspan(field_string)
            tspan.set('x', horizontal_offset)
            tspan.set('y', tspan_y)
            text_element.append(tspan)
            tspan_y += (self.font_size + vertical_spacing)

    return text_element


def get_file_modified_timestamp(self, filepath):
    timestamp = os.path.getmtime(filepath)

    file_timestamp = datetime.fromtimestamp(timestamp)

    return file_timestamp


class PrintMetadata(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--font_size_float", type=float, dest="font_size_float", default=10)
        pars.add_argument("--datetimestamp_cb", type=inkex.Boolean, dest="datetimestamp_cb", default=True)

    def effect(self):
        # Current svg save location (if has been saved)
        document_path = self.document_path()

        # Get file modification date and time
        if document_path:
            file_timestamp = get_file_modified_timestamp(self, document_path).isoformat(sep="_", timespec="seconds")
            self.file_timestamp = file_timestamp

        selection_list = self.svg.selected
        if len(selection_list) > 0:
            if selection_list[0].TAG == 'path':
                path = selection_list[0]
            else:
                path = None
        else:
            path = None

        # Make a dictionary from metadata
        metadata_dict = get_metadata(self)

        # Set the font size and append to self
        self.font_size = self.options.font_size_float

        text_element = create_text(self, metadata_dict, path)

        # Append to a new layer
        metadata_layer = Layer()
        metadata_layer.append(text_element)
        metadata_layer.label = 'Metadata Layer'
        self.svg.append(metadata_layer)


if __name__ == '__main__':
    PrintMetadata().run()
